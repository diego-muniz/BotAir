﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;

namespace BotAir.Dialogs
{
  [LuisModel("fc275796-a9a3-4fb4-baa7-c3aa419c0d5a", "a92e01c615e34adeb07ef5bf237d7116")]
  [Serializable]
  public class RootDialog : LuisDialog<object>
  {

    [LuisIntent("None")]
    public async Task None(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Não entendi...O que você quer dizer ?");
    }

    [LuisIntent("Cumprimento")]
    public async Task Cumprimento(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Olá eu me chamo BotAir, como posso te ajudar ?");
    }


    [LuisIntent("Scrum")]
    public async Task Scrum(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("O nome Scrum vem de uma formação de Rubgy, um esporte coletivo originário da Inglaterra.Scrum ou formação ordenada é uma situação frequente no rugby, geralmente usada após uma jogada irregular ou em alguma penalização. Nesta formação, 8 jogadores de cada time devem se encaixar para formar uma muralha.Nesta formação é muito importante que seja realizado um trabalho de equipe, pois se um dos jogadores na formação falhar, toda a jogada será comprometida.O Scrum não é um processo previsível, ele não define o que fazer em toda circunstância (Ken Shwaber, 2004).");
    }
    
    [LuisIntent("Scrum Master")]
    public async Task ScrumMaster(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("SCRUM MASTER - Responsável em manter o Time dentro do fluxo Scrum e removendo quaisquer impedimentos que possam interferir no objetivo do Time Scrum. Ajuda o Time a entender e usar o autogerenciamento, focando sempre nos pilares do Scrum: Transparência, inspeção e adaptação!");
    }

    [LuisIntent("Product Owner")]
    public async Task ProductOwner(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("PRODUCT OWNER - Conhecido como PO.Responsável principalmente pelo Backlog do Produto e garantir o ROI, valor entregue ao cliente.Defendendo - o de influências externas ao produto e garantindo que o Time entenda o valor a ser entregue.");
    }

    [LuisIntent("Team Development")]
    public async Task TeamDevelopment(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Time de desenvolvimento - Composto entre 3 e 9 integrantes, o Time de Desenvolvedores é responsável por transformar itens do Product Backlog em incremento ao cliente. São auto organizáveis e não se recomenda alterações no Time, isso pode provocar perca de produtividade.");
    }

    [LuisIntent("Sprint")]
    public async Task Sprint(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Sprint - Time-box de 1 a 4 semanas com uma meta estabelecida e com um objetivo claro.Deve se manter constante e abrange o Planejamento da Sprint, Daily, Sprint Review e Sprint Retrospectiva.Ao final de cada Sprint, deve-se entregar um incremento.");
    }

    [LuisIntent("Sprint Planning")]
    public async Task SprintPlanning(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Ocorre no início de cada Sprint.Time-box de 8 Horas para Sprint de 1 mês, dividida em duas partes onde na primeira é definida o que deve ser feito e na segunda, como fazer. Nesta reunião é criado o Sprint Backlog.");
    }

    [LuisIntent("Daily")]
    public async Task Daily(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Daily - Com foco nos pilares do Scrum: transparência, inspeção e adaptação, a reunião diária tem como característica 3 perguntas: o que eu fiz ontem? O que pretendo fazer hoje? Há algum impedimento para realizar minha tarefa? Time-box de 15 min, sendo uma reunião com base na conversa e alinhamento das demandas.");
    }

    [LuisIntent("Sprint Review")]
    public async Task SprintReview(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Sprint Review - Time-box de 4 horas para Sprint de 1 mês, tem por objetivo apresentar o incremento produzido dentro da Sprint às partes interessadas.Observando o que foi planejado e posto como objetivo e meta no planejamento da Sprint.");
    }

    [LuisIntent("Sprint Retrospective")]
    public async Task SprintRetrospective(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Sprint Retrospective - É o último evento da Sprint.Com cerca de 3 horas para Sprint de 1 mês, tem como objetivo a revisão das práticas utilizadas para desenvolver o incremento. O que foi bom? O que precisa melhorar? O que devemos descartar que não foi produtivo? Nesta reunião é importante que o Time inteiro esteja presente!.");
    }


  }
}